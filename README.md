# ThermoSmart plugin for Homebridge

![ThermoSmart Homebridge](homebridge-ui/public/images/thermosmart_homebridge.png)

This is a plugin for [Homebridge](https://github.com/homebridge/homebridge) to allow you to control your
[ThermoSmart](https://www.thermosmart.com/) thermostat using iOS HomeKit.

This plugin is not affiliated with or endorsed by ThermoSmart B.V.

## Usage within Home app

This Homebridge accessory has the same functionality as the ThermoSmart thermostat: you can manually control the target
temperature and pause and unpause the program. It cannot be used to change the schedule, exceptions or standard week, for
that you'll need to use the [ThermoSmart web portal](https://my.thermosmart.com/). The temperature display units setting
(Celsius of Fahrenheit) doesn't do anything, as far as I can tell, the ThermoSmart themostat doesn't support Fahrenheit.

## Prerequisites

### ThermoSmart API Access

You'll need (personal) API access for your ThermoSmart, which you can request using [the form on their website](https://www.thermosmart.com/product/api/).
After requesting API access, you'll receive a client ID and a client secret, which you'll need to grant the Homebridge plugin
access to you ThermoSmart.

### Webhook Server

For immediate updates from your thermostat to HomeKit, you can supply a webhook url with your API access request. By default,
a default built-in webserver is started which listens on `127.0.0.1:7512`. How this server can be reached from the outside
world depends on your home network setup. If the Homebridge UI is reachable from the the internet, you can add a `proxy_pass`
rule to the nginx configuration that redirects incoming traffic to `127.0.0.1:7512`.

If the webhook server isn't reachable from the outside world or if it's disabled (see [advanced configuration](#advanced-configuration)
below), the plugin will poll for new data at an interval of twice the TTL (which defaults to 5 minutes, so the data will be
refreshed every 10 minutes by default).

The webhook server listens for `POST` requests to `/events`, so you might want to add an extra url path component to not
interfere with the Homebridge url space. For example, to use the webhook url `https://example.com/thermosmart/events`, you
can use the following nginx configuration (add before the `location /` used by the Homebridge Web UI, mind the trailing
slashes after `/thermosmart/` and `http://127.0.0.1:7512/`):

```nginx
location /thermosmart/ {
  proxy_pass                  http://127.0.0.1:7512/;
  proxy_http_version          1.1;
  proxy_buffering             off;
  proxy_set_header            Host $host;
  proxy_set_header            Upgrade $http_upgrade;
  proxy_set_header            Connection "Upgrade";
  proxy_set_header            X-Real-IP $remote_addr;
  proxy_set_header            X-Forwarded-For $proxy_add_x_forwarded_for;
}
```

You can also configure the webhook server to listen on an externally accessible IP address and port number (see
[advanced configuration](#advanced-configuration) below). In this case, you would use the `/events` as the url path, for
example `http://example.org:7512/events`. **Please note that this configuration is not recommended, as the webhook server
doesn't support secure https internet connections!**

The webhook server keeps a (rotating) access log named `webhook.log` in a `thermosmart` within the homebridge storage
directory (`/var/lib/homebridge` by default).

#### Webhook server proxy configuration on Homebridge Raspberry Pi image

If you're using the Homebridge Raspberry Pi image, the `proxy_pass` rule will be overwritten each time Homebridge restart. To
prevent the webhook configuration from being overwritten, you can update `/etc/hb-service/homebridge/prestart.d/20-hb-nginx-check`
by changing the regular expression on the line containing the `sed` command near the end to start with `/#keep/! `, so it
will look something like this:

```shell
  sed -i "/#keep/! /proxy_pass/c\    proxy_pass                  http://127.0.0.1:${UI_PORT};" /etc/nginx/sites-available/homebridge.local
```

and then appending `#keep` to you webhook's `proxy_pass` configuration:

```nginx
location /thermosmart/ {
  proxy_pass                  http://127.0.0.1:7512/; #keep
```

### Homebridge

Make sure you have Homebridge up and running.

## Installation

The easiest way to install and configure this plugin is through the [Homebridge UI](https://www.npmjs.com/package/homebridge-config-ui-x),
but you can also install and  configure the platform manually.

### Homebridge UI

These instructions are for the new custom UI, which is only available on Homebridge UI version v4.34.0 or newer. If you have
an older version of Homebrige UI, you can either upgrade or follow the
[old-style Homebridge UI configuration](#homebridge-ui-old-style-configuration) instructions.

* Install the plugin by searching for `thermosmart` in the Homebridge UI's 'Plugins' section and clicking the 'Install' action.
* Open the ThermoSmart settings and choose 'Start' to start adding your ThermoSmart themostat
* Choose a name (this is the name that is used to refer to this thermostat in the interface and the default name that shows
  up in HomeKit)
* Fill in you client id and secret and choose 'Next'
* Choose 'Start Authentication'
* A popup window will open in which you login to your ThermoSmart account and give access to the HomeBridge plugin
* If successful, the popup window closes and an authorization code is shown
* Choose 'Next' to retrieve an access token using the authorization code
* If successful, you'll see your ThermoSmart's hardware ID, an access token and a token type (which should be 'Bearer')
* Choose 'Save config' to save the configuration
* Close the window and restart Homebridge to make your thermostat available to HomeKit

### Homebridge UI old-style configuration

* Install the plugin by searching for `thermosmart` in the Homebridge UI's 'Plugins' section and clicking the 'Install' action.
* Get an access token by running `thermosmart-accesstoken` in a console
* Open the ThermoSmart settings
* Choose a thermostat name (this is the name that is used to refer to this thermostat in the interface and the default name
  that shows up in HomeKit)
* Fill in you client id and secret
* Open the 'Authorization' settings
* Set the hardware ID, access token and token type to the `thermostat`, `access_token` and `token_type` values from the token
  you received from the `thermosmart-accesstoken` script in the first step
* Save the settings and restart Homebridge to make your thermostat available to HomeKit

### Manual configuration

* Install the plugin by running `npm i -g homebridge-thermosmart`
* Get an access token by running `thermosmart-accesstoken`
* Add the ThermoSmart plugin to the platforms in the Homebridge configuration:
  * The platform `name` is the name of the plugin in Homebridge
  * The thermostat `name` is the name of your thermostat in HomeKit
  * the `token` is what you received from the `accesstoken` script

```json
"platforms": [
    {
        "name": "ThermoSmart",
        "platform": "HomebridgeThermoSmart",
        "thermostats": [
            {
                "name": "My ThermoSmart",
                "token": {
                    "thermostat": "[hardware id]",
                    "access_token": "[access token]",
                    "token_type": "Bearer"
                }
            }
        ]
    }
]
```

* Save the configuration file and restart Homebridge to make your thermostat available to HomeKit

### Advanced configuration

There are some advanced API settings that usually don't have to be changed. They can only be configured through the
configuration file. These are the default settings:

```json
"platforms": [
    {
        "name": "ThermoSmart",
        "platform": "HomebridgeThermoSmart",
        "thermostats": [...],
        "api": {
            "url": "https://api.thermosmart.com",
            "ttl": 5,
            "port": 7512,
            "ip": "127.0.0.1"
        }
    }
]
```

* `url`: The API base url.
* `ttl`: The time (in minutes) that thermostat data should be cached when retrieving it from the API to make sure the API
  doesn't get called too often. If the webhook is configured, the data will be kept up to date by the incoming webhook calls.
* `port`: The port the webhook server is listening on (see also [the webhook configuration](#webhook-server) above). Set to 0
  to disable the webhook listener.
* `ip`: The IP address the webhook server is listening on (see also [the webhook configuration](#webhook-server) above).

### Development

* Clone the repository: `git clone git@gitlab.com:krisbreuker/homebridge-thermosmart.git`
* Change into the cloned repository: `cd homebridge-thermosmart`
* Install the required packages: `npm i`
* Compile the package: `npm run build`
* Make it globally available available to Homebridge: `npm link` (or `sudo npm link` if necessary)

After making changes to the source code, recompile using `npm run build` and restart Homebridge.

## Acknowledgements

The thermostat OAuth configuration flow is based on the [Homebridge Mercedes Me](https://github.com/seydx/homebridge-mercedesme)
plugin.
