const { HomebridgePluginUiServer, RequestError } = require("@homebridge/plugin-ui-utils");
const { AuthorizationCode } = require("simple-oauth2");


const TS_APIURL = "https://api.thermosmart.com";
const TS_AUTHPATH = "/oauth2/authorize";
const TS_TOKENPATH = "/oauth2/token";
const SCOPE = "ot"; // OpenTherm


class UiServer extends HomebridgePluginUiServer {
  constructor () {
    super();

    this.onRequest("/authCode", config => this.authCode(config));
    this.onRequest("/authToken", config => this.authToken(config));

    this.client = null;

    this.ready();
  }

  async authCode(config) {
    this.client = null;

    let tokenHost = config.apiURL || TS_APIURL;
    if (tokenHost.endsWith("/"))
      tokenHost = tokenHost.substr(0, tokenHost.length - 1);
    let authorizePath = TS_AUTHPATH;
    let tokenPath = TS_TOKENPATH;

    // If the API URL contains a path after the host, prepend the authorize and token paths with the API path prefix
    let idx = tokenHost.indexOf("://");
    if (idx < 0)
      throw new RequestError(`Invalid API URL '${tokenHost}'`);
    idx = tokenHost.indexOf("/", idx + 3);
    if (idx > 0) {
      const prefix = tokenHost.substr(idx);
      if (prefix) {
        authorizePath = prefix + authorizePath;
        tokenPath = prefix + tokenPath;
      }
      tokenHost = tokenHost.substr(0, idx);
    }

    const params = {
      client: {
        id: config.clientID,
        secret: config.clientSecret
      },
      auth: {
        tokenHost,
        authorizePath,
        tokenPath
      }
    };

    const redirect_uri = config.origin;

    this.client = new AuthorizationCode(params);

    return this.client.authorizeURL({
      redirect_uri: redirect_uri,
      scope: SCOPE
    });
  }

  async authToken(config) {
    const code = config.autherization_code;

    const options = {
      code,
      redirect_uri: config.origin,
      scope: SCOPE
    };

    try {
      return await this.client.getToken(options);
    } catch (err) {
      throw new RequestError(err.message);
    }
  }
}

(() => {
  return new UiServer;
})();
