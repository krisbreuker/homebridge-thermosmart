/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "schema" }]*/

const schema = {
  "schema": {
    "name": {
      "title": "Plugin Name",
      "description": "The name of the plugin in Homebridge",
      "type": "string",
      "default": "ThermoSmart"
    },
    "thermostats": {
      "type": "object",
      "properties": {
        "name": {
          "title": "Thermostat Name",
          "description": "The name of your thermostat in HomeKit",
          "type": "string",
          "required": true
        },
        "clientID": {
          "title": "Client ID",
          "description": "Client ID for the ThermoSmart API",
          "type": "string",
          "required": true
        },
        "clientSecret": {
          "title": "Client Secret",
          "description": "Client Secret for the ThermoSmart API",
          "type": "string",
          "required": true
        },
        "token": {
          "titel": "Token",
          "type": "object",
          "properties": {
            "thermostat": {
              "title": "Hardware ID",
              "description": "The hardware ID of your thermostat",
              "type": "string",
              "required": true
            },
            "access_token": {
              "title": "Access Token",
              "type": "string",
              "required": true
            },
            "token_type": {
              "title": "Token Type",
              "type": "string",
              "required": true
            }
          }
        }
      }
    }
  },
  "layout": [
    "name",
    "thermostats.name",
    "thermostats.clientID",
    "thermostats.clientSecret",
    {
      "key": "thermostats.token",
      "type": "section",
      "title": "Authorization",
      "expandable": true,
      "expanded": false,
      "orderable": false,
      "items": [
        "thermostats.token.thermostat",
        "thermostats.token.access_token",
        "thermostats.token.token_type"
      ]
    }
  ]
};
