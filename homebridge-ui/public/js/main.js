/*global $, window, location, homebridge, schema*/

const GLOBAL = {
  pluginConfig: null,
  customSchema: null,
  thermostatOptions: null,
  currentContent: null,
  previousContent: [],
};

function toggleContent() {
  $("#header").hide();
  $("#main").show();
}

function transPage(cur, next, removed, showSchema) {
  if (showSchema) {
    cur.hide();
    next.show();

    //GLOBAL.previousContent.push($("#isConfigured"));
    GLOBAL.previousContent.push(cur);
    GLOBAL.currentContent = next;

    return;
  }
  toggleContent();

  if (cur) {
    cur.fadeOut(500, () => {
      next.fadeIn(500);

      if (!removed)
        GLOBAL.previousContent.push(cur);

      GLOBAL.currentContent = next;
    });
  } else {
    next.fadeIn(500);

    if (!removed)
      GLOBAL.previousContent.push(next);

    GLOBAL.currentContent = next;
  }

  if (GLOBAL.customSchema)
    GLOBAL.customSchema.end();

  homebridge.hideSchemaForm();
}

function goBack(index) {
  if (GLOBAL.previousContent.length && GLOBAL.currentContent) {
    index = index === undefined
      ? GLOBAL.previousContent.length - 1
      : index;

    transPage(GLOBAL.currentContent, GLOBAL.previousContent[index], true);
    //GLOBAL.currentContent = GLOBAL.previousContent[index];
    GLOBAL.previousContent.splice(index, 1);

    if (GLOBAL.customSchema)
      GLOBAL.customSchema.end();
  }
}

async function createCustomSchema(thermostat) {
  GLOBAL.thermostatOptions = {
    name: thermostat.name,
    clientID: thermostat.clientID,
    clientSecret: thermostat.clientSecret,
    apiURL: GLOBAL.pluginConfig[0].api?.url,
    origin: location.origin
  };

  GLOBAL.customSchema = homebridge.createForm(schema, {
    name: GLOBAL.pluginConfig[0].name,
    thermostats: thermostat
  });

  GLOBAL.customSchema.onChange(async config => {
    GLOBAL.pluginConfig[0].name = config.name;
    GLOBAL.pluginConfig[0].thermostats = GLOBAL.pluginConfig[0].thermostats.map(thermostat => {
      if (thermostat.name === config.thermostats.name) {
        thermostat = config.thermostats;
      }
      return thermostat;
    });

    try {
      await homebridge.updatePluginConfig(GLOBAL.pluginConfig);
    } catch(err) {
      homebridge.toast.error(err.message, "Error");
    }
  });
}

function resetUI() {
  resetForm();
  resetSchema();
}

function resetForm() {
  $("#thermostatName").val("");
  $("#thermostatClientID").val("");
  $("#thermostatClientSecret").val("");
  $("#authCode").val("");
  $("#thermostat").val("");
  $("#authToken").val("");
  $("#authTokenType").val("");

  $("#codeInput").hide();
  $("#tokenInput").hide();

  GLOBAL.thermostatOptions = null;
}

function resetSchema() {
  if (GLOBAL.customSchema) {
    GLOBAL.customSchema.end();
    GLOBAL.customSchema = null;
  }
}

function addThermostatToList(thermostat) {
  let name = typeof thermostat === "string" ? thermostat : thermostat.name;
  $("#thermostatSelect").append(`<option value="${name}">${name}</option>"`);
}

function removeThermostatFromList(thermostat) {
  let name = typeof thermostat === "string" ? thermostat : thermostat.name;
  $(`#thermostatSelect option[value="${name}"]`).remove();
}

async function addNewDeviceToConfig(thermostat) {
  let found = false;
  try {
    const config = {
      name: thermostat.name,
      clientID: thermostat.clientID,
      clientSecret: thermostat.clientSecret,
      token: {
        thermostat: thermostat.token.thermostat,
        access_token: thermostat.token.access_token,
        token_type: thermostat.token.token_type,
      }
    };

    for (const thermostatt in GLOBAL.pluginConfig[0].thermostats) {
      if (GLOBAL.pluginConfig[0].thermostats[thermostatt].name === thermostat.name) {
        found = true;
        GLOBAL.pluginConfig[0].thermostats[thermostatt].token = {
          thermostat: thermostat.token.thermostat,
          access_token: thermostat.token.access_token,
          token_type: thermostat.token.token_type,
        };
        homebridge.toast.success(thermostat.name + " refreshed!", "Success");
      }
    }

    if (!found) {
      GLOBAL.pluginConfig[0].thermostats.push(config);
      addThermostatToList(config);

      homebridge.toast.success(config.name + " added to config!", "Success");
    }

    await homebridge.updatePluginConfig(GLOBAL.pluginConfig);
    await homebridge.savePluginConfig();
  } catch(err) {
    homebridge.toast.error(err.message, "Error");
  }
}

async function removeDeviceFromConfig() {
  let foundIndex;
  let pluginConfigBkp = GLOBAL.pluginConfig;
  let selectedThermostat = $("#thermostatSelect option:selected").text();

  GLOBAL.pluginConfig[0].thermostats.forEach((thermostat, index) => {
    if (thermostat.name === selectedThermostat) {
      foundIndex = index;
    }
  });

  if (foundIndex !== undefined) {
    try {
      GLOBAL.pluginConfig[0].thermostats.splice(foundIndex, 1);

      await homebridge.updatePluginConfig(GLOBAL.pluginConfig);
      await homebridge.savePluginConfig();

      removeThermostatFromList(selectedThermostat);

      homebridge.toast.success(selectedThermostat + " removed from config!", "Success");
    } catch(err) {
      GLOBAL.pluginConfig = pluginConfigBkp;

      throw err;
    }
  } else {
    throw new Error("No thermostat found in config to remove!");
  }
}

(async () => {
  try {
    GLOBAL.pluginConfig = await homebridge.getPluginConfig();

    if (!GLOBAL.pluginConfig.length) {

      GLOBAL.pluginConfig = [ {
          platform: "HomebridgeThermoSmart",
          name: "ThermoSmart",
          thermostats: []
      } ];

      transPage(false, $("#notConfigured"));
    } else {
      if (!GLOBAL.pluginConfig[0].thermostats || (GLOBAL.pluginConfig[0].thermostats && !GLOBAL.pluginConfig[0].thermostats.length)) {
        GLOBAL.pluginConfig[0].thermostats = [];
        return transPage(false, $("#notConfigured"));
      }

      GLOBAL.pluginConfig[0].thermostats.forEach(thermostat => {
        $("#thermostatSelect").append(`<option value="${thermostat.name}">${thermostat.name}</option>`);
      });

      transPage(false, $("#isConfigured"));
    }
  } catch(err) {
    homebridge.toast.error(err.message, "Error");
  }
})();

//jquery listener

$(".back").on("click", () => {
  goBack();
});

$("#addThermostat, #start").on("click", () => {
  resetUI();

  let activeContent = $("#notConfigured").css("display") !== "none" ? $("#notConfigured") : $("#isConfigured");

  transPage(activeContent, $("#configureThermostat"));
});

$("#auth").on("click", () => {
  try {
    GLOBAL.thermostatOptions = {
      name: $("#thermostatName").val(),
      clientID: $("#thermostatClientID").val(),
      clientSecret: $("#thermostatClientSecret").val(),
      apiURL: GLOBAL.pluginConfig[0].api?.url,
      origin: location.origin
    };

    let thermostatConfig = GLOBAL.pluginConfig[0].thermostats.find(thermostat => thermostat && thermostat.name === GLOBAL.thermostatOptions.name);

    if (thermostatConfig) {
      return homebridge.toast.error("There is already a thermostat configured with the same name!", "Error");
    } else if (!GLOBAL.thermostatOptions.name) {
      return homebridge.toast.error("There is no name configured for this thermostat!", "Error");
    } else if (!GLOBAL.thermostatOptions.clientID) {
      return homebridge.toast.error("There is no client ID configured for this thermostat!", "Error");
    } else if (!GLOBAL.thermostatOptions.clientSecret) {
      return homebridge.toast.error("There is no client secret configured for this thermostat!", "Error");
    }

    transPage($("#configureThermostat"), $("#authentication"));
  } catch(err) {
    homebridge.toast.error(err.message, "Error");
  }
});

$("#startAuth").on("click", async () => {
  try {
    homebridge.showSpinner();

    GLOBAL.thermostatOptions.authorizationUri = await homebridge.request("/authCode", GLOBAL.thermostatOptions);

    const win = window.open(GLOBAL.thermostatOptions.authorizationUri, "windowname1", "width=800, height=600");

    const pollTimer = window.setInterval(function() {
      if (win.document.URL.includes("?code=")) {
        window.clearInterval(pollTimer);
        GLOBAL.thermostatOptions.autherization_code = win.document.URL.split("?code=")[1];
        $("#authCode").val(GLOBAL.thermostatOptions.autherization_code);
        win.close();
        homebridge.hideSpinner();
        $("#codeInput").fadeIn();
      }
    }, 1000);
  } catch(err) {
    homebridge.hideSpinner();
    homebridge.toast.error(err.message, "Error");
  }
});

$("#generateToken").on("click", async () => {
  try {
    homebridge.showSpinner();

    GLOBAL.thermostatOptions.token = await homebridge.request("/authToken", GLOBAL.thermostatOptions);

    //TODO: Check if we already have a thermostat with this thermostat id!

    $("#thermostat").val(GLOBAL.thermostatOptions.token.thermostat);
    $("#authToken").val(GLOBAL.thermostatOptions.token.access_token);
    $("#authTokenType").val(GLOBAL.thermostatOptions.token.token_type);

    homebridge.hideSpinner();

    $("#tokenInput").fadeIn();
  } catch(err) {
    homebridge.hideSpinner();
    homebridge.toast.error(err.message, "Error");
  }
});

$("#saveAuth").on("click", async () => {
  try {
    await addNewDeviceToConfig(GLOBAL.thermostatOptions);

    transPage($("#authentication"), $("#isConfigured"));
  } catch(err) {
    homebridge.toast.error(err.message, "Error");
  }
});

$("#editThermostat").on("click", () => {
  resetUI();

  let selectedThermostat = $( "#thermostatSelect option:selected" ).text();
  let thermostat = GLOBAL.pluginConfig[0].thermostats.find(thermostat => thermostat.name === selectedThermostat);

  if (!thermostat) {
    return homebridge.toast.error("Can not find the thermostat!", "Error");
  }

  createCustomSchema(thermostat);

  transPage($("#main, #isConfigured"), $("#header"), false, true);
});

$("#refreshThermostat").on("click", async () => {
  if (GLOBAL.customSchema && GLOBAL.thermostatOptions) {
    resetSchema();

    let thermostat = GLOBAL.pluginConfig[0].thermostats.find(thermostat => thermostat.name === GLOBAL.thermostatOptions.name);

    if (!thermostat) {
      return homebridge.toast.error("Can not find thermostat in config!", "Error");
    }

    transPage($("#isConfigured"), $("#authentication"));
  }
});

$("#removeThermostat").on("click", async () => {
  try {
    await removeDeviceFromConfig();

    resetUI();

    transPage(false, GLOBAL.pluginConfig[0].thermostats.length ? $("#isConfigured") : $("#notConfigured"));
  } catch (err) {
    homebridge.toast.error(err.message, "Error");
  }
});
