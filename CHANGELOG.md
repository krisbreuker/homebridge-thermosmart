# Changelog

## 1.1.2

* Prevent the refresh timeout from being set multiple times

## 1.1.1

* Fix initializing webhook server

## 1.1.0

* Poll for updates if webhook is not configured or reachable

## 1.0.2

* NPM updates
* Bugfix in reading webhook configuration

## 1.0.1

* NPM updates

## 1.0.0

* Initial Release
