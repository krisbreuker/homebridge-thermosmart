#!/bin/sh
":" //# comment; exec /usr/bin/env node --experimental-modules "$0" "$@"
import { argv, exit, stderr, stdin, stdout } from "process";
import { clearLine, createInterface, moveCursor } from "readline";
import { URL } from "url";
import httpPromise from "./lib/httppromise.mjs";

let debug = argv.includes("--debug") || argv.includes("-d");
if (debug) {
  console.info("Debugging on");
}

async function questionPromise(rl, query, masked = false) {
  return new Promise(resolve => {
    rl.stdoutMasked = masked;
    rl.question(query, answer => {
      rl.stdoutMasked = false;
      if (masked) {
        rl.history = rl.history.slice(1);
      }
      resolve(answer);
    });
  });
}

async function main() {
  // Create a readline interface
  const rl = createInterface({ input: stdin, output: stdout });
  // Mask password characters with '*' if 'stdoutMasked' is set
  rl.input.on("keypress", () => {
    if (rl.stdoutMasked) {
      var len = rl.line.length;
      moveCursor(rl.output, -len, 0);
      clearLine(rl.output, 1);
      for (var i = 0; i < len; i++) {
        rl.output.write("*");
      }
    }
  });

  // Ask login data
  stdout.write(`This tool can be used to retrieve an access token for the ThermoSmart
Homebridge plugin if you don't use the Homebridge UI plugin. Your
login data and API details are only used to retrieve the access token
and aren't stored anywhere. You'll only need the resulting token to
add to your platform configuration. See the README for more details.

Please provide the login details for your ThermoSmart account:
`);
  const username = await questionPromise(rl, "Email: ");
  const password = await questionPromise(rl, "Password: ", true);
  stdout.write(`
Please provide the client details for you API access:
`);
  const clientid = await questionPromise(rl, "Client ID: ");
  const secretkey = await questionPromise(rl, "Secret key: ", true);
  rl.close();

  // Base settings
  const redirect_uri = "http://clientapp.com/done";
  const url = new URL("https://api.thermosmart.com");
  const scope = "ot";

  // Login
  stdout.write(`
Logging in...`);
  url.pathname = "/login";
  let response = await httpPromise(url, {
    method: "POST",
    vars: {
      password,
      username
    },
    debug
  });
  // After successful login, we should be redirected to '/'
  if (response.status != 302 || response.headers["location"] != "/") {
    stderr.write(`
Error while logging in, check your login details
`);
    exit(1);
  }
  const cookie = response.headers["set-cookie"].map(_ => _.split(";")[0]).join("; ");

  // Get Authorize Dialog
  moveCursor(stdout, -13, 0);
  clearLine(stdout, 1);
  stdout.write("Getting transation id...");
  url.pathname = "/oauth2/authorize";
  response = await httpPromise(url, {
    method: "GET",
    headers: { cookie },
    vars: {
      client_id: clientid,
      redirect_uri,
      response_type: "code",
      scope
    },
    debug
  });
  // This requests returns the authorization dialog html
  if (response.status != 200) {
    stderr.write(`
Error while getting authorization dialog
`);
    exit(1);
  }
  // Ugly code to quickly find the transaction id without having to parse html
  const match = response.body.match(/<input name="transaction_id" type="hidden" value="([^"]+)">/);
  if (!match) {
    stderr.write(`
Couldn't find authorization dialog
`);
    exit(1);
  }
  // match[0] is the complete input, match[1] is the first capture group
  const transaction_id = match[1];

  // Authorize
  moveCursor(stdout, -24, 0);
  clearLine(stdout, 1);
  stdout.write("Authorizing...");
  url.pathname = "/oauth2/authorize/decision";
  response = await httpPromise(url, {
    method: "POST",
    headers: { cookie },
    vars: { transaction_id },
    debug
  });
  // We should be redirected to our redirect_uri, which should have the 'code' url variable set
  if (response.status != 302 || !response.headers["location"].startsWith(redirect_uri)) {
    stderr.write(`
Not authorized
`);
    exit(1);
  }
  const code = new URL(response.headers["location"]).searchParams.get("code");
  if (!code) {
    stderr.write(`
Didn't receive authorization code
`);
    exit(1);
  }

  // Exchange authorization code for Access token
  moveCursor(stdout, -14, 0);
  clearLine(stdout, 1);
  stdout.write("Getting access token...");
  url.pathname = "/oauth2/token";
  response = await httpPromise(url, {
    method: "POST",
    headers: {
      "authorization": `Basic ${Buffer.from(`${clientid}:${secretkey}`, "utf-8").toString("base64")}`,
      cookie
    },
    vars: {
      code,
      grant_type: "authorization_code",
      redirect_uri,
      scope
    },
    debug
  });
  // We should receive a JSON encoded access token
  if (response.status != 200 || response.headers["content-type"] != "application/json") {
    stderr.write(`
Error retrieving access token
`);
    exit(1);
  }
  const token = response.json;

  moveCursor(stdout, -23, 0);
  clearLine(stdout, 1);
  stdout.write(`You can add this token to your platform configuration:
`);
  console.log(token);
  stdout.write(`
You should now be able to retrieve your ThermoSmart data using:
curl https://api.thermosmart.com/thermostat/${token.thermostat}?access_token=${token.access_token}
`);
}

main();
