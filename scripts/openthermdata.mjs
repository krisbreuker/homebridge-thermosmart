import { argv, exit, stderr } from "process";
import httpPromise from "./lib/httppromise.mjs";

if (argv.length < 4) {
  stderr.write("Usage: npm run openthermdata <hardwareid> <access_token>\n");
  exit(1);
}

function readFloat(value) {
  if (value == "0xdead") {
    return null;
  }
  return parseInt(value) / 256;
}

async function main() {
  const url = new URL("https://api.thermosmart.com");
  const hw = argv[2];
  const access_token = argv[3];

  url.pathname = `/thermostat/${hw}`;
  const response = await httpPromise(url, { vars: { access_token } });
  if (!response?.json?.ot?.enabled) {
    stderr.write("Didn't receive OpenTherm data\n");
    exit(1);
  }

  const raw = response.json.ot.raw;
  const ot = {
    ch_enabled: false,
    dhw_enabled: false,
    cooling_enabled: false,
    otc_active: false,
    ch2_enabled: false,
    summer_mode: false,
    dhw_blocking: false,
    fault: false,
    ch_active: false,
    dhw_active: false,
    flame_on: false,
    cooling_active: false,
    ch2_active: false,
    diagnostic_service: false,
    electricity: false,
    control_setpoint: null,
    dhw_present: false,
    on_off_mode: false,
    cooling_supported: false,
    dhw_storage_tank: false,
    master_low_off_not_allowed: false,
    ch2_present: false,
    remote_water_unavailable: false,
    mode_switching_slave: false,
    slave_memberid: null,
    modulation_level: null,
    water_pressure: null,
    dhw_flow_rate: null,
    boiler_temperature: null,
    dhw_temperature: null,
    outside_temperature: null,
    return_water_temperature: null,
    boiler_heat_exchanger_temperature: null,
    dhw_setpoint: null,
    opentherm_version_slave: null
  }

  // ot0
  const master_status = parseInt(raw.ot0) >> 8;
  ot.ch_enabled = (master_status & 0b00000001) > 0;
  ot.dhw_enabled = (master_status & 0b00000010) > 0;
  ot.cooling_enabled = (master_status & 0b00000100) > 0;
  ot.otc_active = (master_status & 0b00001000) > 0;
  ot.ch2_enabled = (master_status & 0b00010000) > 0;
  ot.summer_mode = (master_status & 0b00100000) > 0;
  ot.dhw_blocking = (master_status & 0b01000000) > 0;
  const slave_status = parseInt(raw.ot0) & 0xff;
  ot.fault = (slave_status & 0b00000001) > 0;
  ot.ch_active = (slave_status & 0b00000010) > 0;
  ot.dhw_active = (slave_status & 0b00000100) > 0;
  ot.flame_on = (slave_status & 0b00001000) > 0;
  ot.cooling_active = (slave_status & 0b00010000) > 0;
  ot.ch2_active = (slave_status & 0b00100000) > 0;
  ot.diagnostic_service = (slave_status & 0b01000000) > 0;
  ot.electricity = (slave_status & 0b10000000) > 0;
  // ot1
  ot.control_setpoint = readFloat(raw.ot1);
  // ot3
  const slave_config = parseInt(raw.ot3) >> 8;
  ot.dhw_present = (slave_config & 0b00000001) > 0;
  ot.on_off_mode = (slave_config & 0b00000010) > 0;
  ot.cooling_supported = (slave_config & 0b00000100) > 0;
  ot.dhw_storage_tank = (slave_config & 0b00001000) > 0;
  ot.master_low_off_not_allowed = (slave_config & 0b00010000) > 0;
  ot.ch2_present = (slave_config & 0b00100000) > 0;
  ot.remote_water_unavailable = (slave_config & 0b01000000) > 0;
  ot.mode_switching_slave = (slave_config & 0b10000000) > 0;
  ot.slave_memberid = parseInt(raw.ot3) & 0xff;
  //ot17
  ot.modulation_level = readFloat(raw.ot17);
  // ot18
  ot.water_pressure = readFloat(raw.ot18);
  // ot19
  ot.dhw_flow_rate = readFloat(raw.ot19);
  // ot25
  ot.boiler_temperature = readFloat(raw.ot25);
  // ot26
  ot.dhw_temperature = readFloat(raw.ot26);
  // ot27
  ot.outside_temperature = readFloat(raw.ot27);
  // ot28
  ot.return_water_temperature = readFloat(raw.ot28);
  // ot34
  ot.boiler_heat_exchanger_temperature = readFloat(raw.ot34);
  // ot56
  ot.dhw_setpoint = readFloat(raw.ot56);
  // ot125
  ot.opentherm_version_slave = readFloat(raw.ot125);

  console.log(ot);
}

main();
