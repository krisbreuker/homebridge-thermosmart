import * as https from "https";

// A custom 'fetch'-like function which works around some limitations with node-fetch (e.g. not allowing all content types
// when not in CORS mode)
export default async function httpPromise(url, options) {
  options = {
    method: "GET",
    headers: {},
    debug: false,
    ...options
  };

  return new Promise((resolve, reject) => {
    // Convert url to string
    if (typeof url != "string") {
      url = url.toString();
    }
    // Set some common headers
    if (!options.headers.accept) {
      options.headers["accept"] = "*/*";
    }
    if (options.method == "POST") {
      options.headers["content-type"] = "application/x-www-form-urlencoded";
    }
    // If there are variables to send, add them to the body when posting or to the url when getting
    if (options.vars) {
      const vars = new URLSearchParams(options.vars).toString();
      if (options.method == "POST") {
        options.body = vars;
      } else {
        url += "?" + vars;
      }
      delete options.vars;
    }
    // Set the content length
    if (options.body) {
      options.headers["content-length"] = Buffer.byteLength(options.body);
    }

    if (options.debug) {
      console.log("req",url,options);
    }

    // Start the request
    const req = https.request(url, options, res => {
      const response = {
        status: res.statusCode,
        headers: {},
        body: "",
        json: null
      };
      // Rewrite the raw array of header names and values to a keyed object
      for (let i = 0; i < res.rawHeaders.length; i += 2) {
        const header = res.rawHeaders[i].toLowerCase();
        if (header in response.headers) {
          if (typeof response.headers[header] == "string")
            response.headers[header] = [ response.headers[header], res.rawHeaders[i+1] ];
          else
            response.headers[header] = [ ...response.headers[header], res.rawHeaders[i+1] ];
        } else {
          response.headers[header] = res.rawHeaders[i+1];
        }
      }
      // Read incoming data
      res.on("data", chunk => response.body += chunk);
      // Wait for response end
      res.on("end", () => {
        if (options.debug) {
          console.log("res", response);
        }
        if ("content-type" in response.headers && response.headers["content-type"].startsWith("application/json")) {
          response.json = JSON.parse(response.body);
        }
        resolve(response);
      });
    }).on("error", error => reject(error));
    // Write the request body
    if (options.body)
      req.write(options.body);
    // Send the request
    req.end();
  });
}
