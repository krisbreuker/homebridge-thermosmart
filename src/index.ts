import {
  API,
  Logging,
  PlatformConfig,
  DynamicPlatformPlugin,
  PlatformAccessory,
} from "homebridge";

import { ThermoSmartAPI } from "./api";
import { PLATFORM_NAME, PLUGIN_NAME } from "./settings";
import { ThermoSmartAccessory } from "./thermosmart";
import { WebhookHandler } from "./webhook";


export = (api: API) => {
  api.registerPlatform(PLATFORM_NAME, ThermoSmartPlatform);
};

class ThermoSmartPlatform implements DynamicPlatformPlugin {

  private readonly log: Logging;
  private readonly config: PlatformConfig;
  private readonly hbAPI: API;
  private readonly accessories: Array<PlatformAccessory>;

  private webhook: WebhookHandler | null = null;

  constructor(log: Logging, config: PlatformConfig, hbAPI: API) {
    this.log = log;
    this.config = config;
    this.hbAPI = hbAPI;
    this.accessories = [];
    if (config.api?.port !== 0) {
      this.webhook = new WebhookHandler(log, hbAPI.user.storagePath(), config.api?.port, config.api?.ip);
    }

    this.hbAPI.on("didFinishLaunching", () => this.initAccessories());
    this.log.info("ThermoSmart finished initializing!");
  }

  private async initAccessories() {
    const activeUUIDs: Array<string> = [];
    const toRegister: Array<PlatformAccessory> = [];
    if (this.config?.thermostats) {
      for (const thermostat of this.config.thermostats) {
        if (!thermostat.token?.thermostat) {
          this.log("Cannot initialize unauthorized accessoy");
          continue;
        }
        const uuid = this.hbAPI.hap.uuid.generate(thermostat.token.thermostat);
        if (!this.accessories.find(accessory => accessory.UUID === uuid)) {
          this.log("Initializing ThermoSmart accessory with UUID", uuid);
          const accessory = new this.hbAPI.platformAccessory(thermostat.name, uuid);
          toRegister.push(accessory);
          accessory.context.thermostat = thermostat.token.thermostat;
          accessory.context.accessToken = thermostat.token.access_token;
          this.createThemorSmartAccessory(accessory);
        } else {
          this.log.debug("Already have accessory with UUID", uuid);
        }
        activeUUIDs.push(uuid);
      }
    }
    const toUnregister: Array<PlatformAccessory> = [];
    for (const accessory of this.accessories) {
      if (!activeUUIDs.includes(accessory.UUID)) {
        this.log("Removing unused ThermoSmart accessory with UUID", accessory.UUID);
        toUnregister.push(accessory);
      }
    }
    this.hbAPI.registerPlatformAccessories(PLUGIN_NAME, PLATFORM_NAME, toRegister);
    try {
      this.hbAPI.unregisterPlatformAccessories(PLUGIN_NAME, PLATFORM_NAME, toUnregister);
    } catch(error) {
      this.log.error(`Error while unregistering accessories: ${error}`);
    }

    if (activeUUIDs.length) {
      if (this.webhook) {
        this.webhook.startServer();
      }
    }
  }

  async configureAccessory(accessory: PlatformAccessory) {
    this.log.debug("configureAccessory", accessory.UUID, accessory.context.thermostat);
    this.accessories.push(accessory);

    if (this.config?.thermostats) {
      for (const thermostat of this.config.thermostats) {
        if (thermostat.token?.thermostat === accessory.context.thermostat) {
          this.log("Restoring ThermoSmart accessory with UUID", accessory.UUID);
          accessory.context.accessToken = thermostat.token?.access_token;
          this.createThemorSmartAccessory(accessory);
          return;
        }
      }
    }
  }

  private createThemorSmartAccessory(accessory: PlatformAccessory) {
    const api = new ThermoSmartAPI(
      this.log,
      this.config.api?.url,
      accessory.context.accessToken,
      accessory.context.thermostat,
      this.config.api?.ttl,
      this.webhook);
    new ThermoSmartAccessory(this.log, this.hbAPI, api, accessory);
  }
}
