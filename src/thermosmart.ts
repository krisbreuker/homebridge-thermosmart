import {
  API,
  CharacteristicEventTypes,
  CharacteristicGetCallback,
  CharacteristicSetCallback,
  CharacteristicValue,
  Logging,
  PlatformAccessory,
  Service,
} from "homebridge";

import { ThermoSmartAPI } from "./api";


export class ThermoSmartAccessory {
  private readonly log: Logging;
  private readonly hbAPI: API;
  private readonly tsAPI: ThermoSmartAPI;
  private readonly accessory: PlatformAccessory;
  private readonly thermostatService: Service;

  constructor(log: Logging, hbAPI: API, tsAPI: ThermoSmartAPI, accessory: PlatformAccessory) {
    this.log = log;
    this.hbAPI = hbAPI;
    this.tsAPI = tsAPI;
    this.accessory = accessory;

    this.log.debug("Initializing ThermoSmart accessory", this.accessory.displayName);

    // Initialize the AccessoryInformation Service
    const informationService = this.accessory.getService(this.hbAPI.hap.Service.AccessoryInformation)
      || this.accessory.addService(this.hbAPI.hap.Service.AccessoryInformation);
    informationService
      .setCharacteristic(this.hbAPI.hap.Characteristic.Manufacturer, "ThermoSmart B.V.")
      //TODO: Can we show the revision of the ThermoSmart as the model (e.g. ThermoSmart Advanced or ThermoSmart V3)?
      .setCharacteristic(this.hbAPI.hap.Characteristic.Model, "ThermoSmart")
      .setCharacteristic(this.hbAPI.hap.Characteristic.SerialNumber, accessory.context.thermostat);
    if (typeof process.env.npm_package_version === "string") {
      informationService.setCharacteristic(this.hbAPI.hap.Characteristic.SoftwareRevision, process.env.npm_package_version);
    }
    //TODO: Can we use this.hbAPI.hap.Characteristic.AppMatchingIdentifier to link to the ThermoSmart app?

    // Read and set firmware version
    this.tsAPI.retrieveFirmwareVersion()
      .then(fw => informationService.setCharacteristic(this.hbAPI.hap.Characteristic.FirmwareRevision, fw))
      .catch(error => this.log.error(`Error while getting firmware version: ${error}`));

    // Initialize the Thermostat Service
    this.thermostatService = this.accessory.getService(this.hbAPI.hap.Service.Thermostat)
      || this.accessory.addService(this.hbAPI.hap.Service.Thermostat);

    this.thermostatService.getCharacteristic(this.hbAPI.hap.Characteristic.CurrentHeatingCoolingState)
      .on(CharacteristicEventTypes.GET, callback => this.getCurrentState(callback));

    this.thermostatService.getCharacteristic(this.hbAPI.hap.Characteristic.TargetHeatingCoolingState)
      .on(CharacteristicEventTypes.GET, callback => this.getTargetState(callback))
      .on(CharacteristicEventTypes.SET, (state, callback) => this.setTargetState(state, callback))
      // Only allow OFF (paused) and HEAT (normal)
      .setProps({ validValues: [
        this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.OFF,
        this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.HEAT,
      ] });

    this.thermostatService.getCharacteristic(this.hbAPI.hap.Characteristic.CurrentTemperature)
      .on(CharacteristicEventTypes.GET, (callback: CharacteristicGetCallback) => this.getCurrentTemperature(callback));

    this.thermostatService.getCharacteristic(this.hbAPI.hap.Characteristic.TargetTemperature)
      .on(CharacteristicEventTypes.GET, (callback: CharacteristicGetCallback) => this.getTargetTemperature(callback))
      .on(CharacteristicEventTypes.SET, (value: CharacteristicValue, callback: CharacteristicSetCallback) => {
        this.setTargetTemperature(value, callback);
      });

    this.tsAPI
      .on("update", value => this.onUpdate(value));
  }

  async getCurrentState(callback: CharacteristicGetCallback) {
    this.log.debug("Get current state");
    try {
      const value = await this.tsAPI.retrieveCentralHeatingActive();
      this.log("Got current state", value ? "HEAT" : "OFF");
      callback(undefined, value
        ? this.hbAPI.hap.Characteristic.CurrentHeatingCoolingState.HEAT
        : this.hbAPI.hap.Characteristic.CurrentHeatingCoolingState.OFF);
    } catch(error) {
      this.log.error(`Error while getting current state: ${error}`);
      callback(error as Error);
    }
  }

  async getTargetState(callback: CharacteristicGetCallback) {
    this.log.debug("Get target state");
    try {
      const value = await this.tsAPI.retrievePause();
      this.log("Got target state", value ? "OFF" : "HEAT");
      callback(undefined, value
        ? this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.OFF
        : this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.HEAT);
    } catch(error) {
      this.log.error(`Error while getting target state: ${error}`);
      callback(error as Error);
    }
  }

  async setTargetState(value: CharacteristicValue, callback: CharacteristicSetCallback) {
    const states: Map<CharacteristicValue, string> = new Map();
    states.set(this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.OFF, "OFF");
    states.set(this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.HEAT, "HEAT");
    states.set(this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.COOL, "COOL");
    states.set(this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.AUTO, "AUTO");
    this.log("Set target state", states.get(value));
    try {
      await this.tsAPI.setPause(value === this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.OFF);
      this.log.debug("Target state set");
      callback();
    } catch(error) {
      this.log.error(`Error while setting target state: ${error}`);
      callback(error as Error);
    }
  }

  async getCurrentTemperature(callback: CharacteristicGetCallback) {
    this.log.debug("Get current temperature");
    try {
      const value = await this.tsAPI.retrieveRoomTemperature();
      this.log("Got current temperature", value);
      callback(undefined, value);
    } catch(error) {
      this.log.error(`Error while getting current temperature: ${error}`);
      callback(error as Error);
    }
  }

  async getTargetTemperature(callback: CharacteristicGetCallback) {
    this.log.debug("Get target temperature");
    try {
      const value = await this.tsAPI.retrieveTargetTemperature();
      this.log("Got target temperature", value);
      callback(undefined, value);
    } catch(error) {
      this.log.error(`Error while getting target temperature: ${error}`);
      callback(error as Error);
    }
  }

  async setTargetTemperature(value: CharacteristicValue, callback: CharacteristicSetCallback) {
    this.log("Set target temperature", value);
    try {
      await this.tsAPI.setTargetTemperature(value as number);
      this.log.debug("Target temperature set");
      callback();
    } catch(error) {
      this.log.error(`Error while setting target temperature: ${error}`);
      callback(error as Error);
    }
  }

  private onUpdate(data: Array<string>) {
    if (data.includes("room_temperature")) {
      const room_temperature = this.tsAPI.getRoomTemperature()!;
      this.log("Update current temperature", room_temperature);
      this.thermostatService.updateCharacteristic(this.hbAPI.hap.Characteristic.CurrentTemperature, room_temperature);
    }
    if (data.includes("target_temperature")) {
      const target_temperature = this.tsAPI.getTargetTemperature()!;
      this.log("Update target temperature", target_temperature);
      this.thermostatService.updateCharacteristic(this.hbAPI.hap.Characteristic.TargetTemperature, target_temperature);
    }
    if (data.includes("pause")) {
      const pause = this.tsAPI.getPause()!;
      this.log("Update target state", pause ? "OFF" : "HEAT");
      this.thermostatService.updateCharacteristic(this.hbAPI.hap.Characteristic.TargetHeatingCoolingState, pause
        ? this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.OFF
        : this.hbAPI.hap.Characteristic.TargetHeatingCoolingState.HEAT);
    }
    if (data.includes("ot")) {
      const heating = this.tsAPI.getCentralHeatingActive()!;
      this.log("Update current state", heating ? "HEAT" : "OFF");
      this.thermostatService.updateCharacteristic(this.hbAPI.hap.Characteristic.CurrentHeatingCoolingState, heating
        ? this.hbAPI.hap.Characteristic.CurrentHeatingCoolingState.HEAT
        : this.hbAPI.hap.Characteristic.CurrentHeatingCoolingState.OFF);
    }
  }
}
