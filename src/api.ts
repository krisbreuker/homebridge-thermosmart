import { EventEmitter } from "events";
import { URL } from "url";

import { Logging } from "homebridge";
import fetch from "node-fetch";

import { WebhookHandler } from "./webhook";


const API_URL = "https://api.thermosmart.com";
const API_TTL = 5; // minutes


interface APIOTData {
  enabled: boolean;
  raw: Record<string, string>;
}

// Relevant API/webhook data
interface UpdateData {
  room_temperature?: number;
  target_temperature?: number;
  source?: string;
  ot?: APIOTData;
}

interface APIResult extends UpdateData {
  name: string;
  outside_temperature: number;
  programs: Record<string, unknown>;
  schedule?: Array<unknown>;
  exceptions?: Array<unknown>;
  fw: string;
  location: string;
  outside_temperature_icon: string;
  geofence_devices: Array<unknown>;
  geofence_enabled: boolean;
}

export interface WebhookData extends UpdateData{
  thermostat: string;
  outside_temperature?: number;
  programs?: Record<string, unknown>;
  schedule?: Array<unknown>;
  exceptions?: Array<unknown>;
}

export interface OpenThermData {
  centralHeatingActive: boolean;
  relativeModulationLevel: number;
}

type OpenThermState = OpenThermData | null;

interface ThermostatData {
  updatedAt: number;
  roomTemperature: number;
  targetTemperature: number;
  firmwareVersion: string;
  pause: boolean;
  openThermData: OpenThermState;
}


export class ThermoSmartAPI extends EventEmitter {
  private readonly log: Logging;
  private readonly apiURL: string;
  private readonly accessToken: string;
  private readonly ttl: number;
  private readonly thermostat: string;

  private thermostatData: ThermostatData | null = null;

  private apiPromise: Promise<void | APIResult | undefined> | null;

  private refreshTimeout: ReturnType<typeof setTimeout> | null = null;

  constructor(log: Logging, apiURL: string, accessToken: string, thermostat: string, ttl: number, webhook: WebhookHandler | null) {
    super();

    this.log = log;
    this.apiURL = apiURL || API_URL;
    this.accessToken = accessToken;
    this.ttl = ttl || API_TTL;
    this.thermostat = thermostat;

    this.apiPromise = this.doAPIRequest().then(result => {
      if (result) {
        this.thermostatData = {
          updatedAt: Date.now(),
          roomTemperature: result.room_temperature!,
          targetTemperature: result.target_temperature!,
          firmwareVersion: result.fw,
          pause: result.source! === "pause",
          openThermData: this.readOpenThermData(result.ot!),
        };
      }

      if (webhook) {
        webhook.on(`webhook:${this.thermostat}`, data => this.onWebhookData(data));
      }
      if (this.ttl) {
        this.refreshTimeout = setTimeout(() => this.refreshThermostat(true), 2 * this.ttl * 60 * 1000);
      }
    }).catch(error => {
      this.log.error(`Error while retrieving thermostat data: ${error}`);
    });
  }

  async initialize() {
    if (this.thermostatData) {
      // Already initialized
      return;
    }
    if (!this.apiPromise) {
      // We're not initialized, but we're also not waiting for the initial API call
      throw new Error("API not properly initialized");
    }
    await this.apiPromise;
    this.apiPromise = null;
  }

  async retrieveFirmwareVersion(): Promise<string> {
    await this.refreshThermostat();
    return this.thermostatData!.firmwareVersion;
  }

  async retrieveCentralHeatingActive(): Promise<boolean | undefined> {
    await this.refreshThermostat();
    return this.getCentralHeatingActive();
  }

  getCentralHeatingActive(): boolean | undefined {
    return this.thermostatData?.openThermData?.centralHeatingActive
      && this.thermostatData?.openThermData?.relativeModulationLevel > 0;
  }

  async retrievePause(): Promise<boolean> {
    await this.refreshThermostat();
    return this.thermostatData!.pause;
  }

  getPause(): boolean | undefined {
    return this.thermostatData?.pause;
  }

  async setPause(value: boolean) {
    await this.initialize();
    await this.doAPIRequest("pause", { pause: value }, "post");
    this.thermostatData!.pause = value;
  }

  async retrieveRoomTemperature(): Promise<number> {
    await this.refreshThermostat();
    return this.thermostatData!.roomTemperature;
  }

  getRoomTemperature(): number | undefined {
    return this.thermostatData?.roomTemperature;
  }

  async retrieveTargetTemperature(): Promise<number> {
    await this.refreshThermostat();
    return this.thermostatData!.targetTemperature;
  }

  getTargetTemperature(): number | undefined {
    return this.thermostatData?.targetTemperature;
  }

  async setTargetTemperature(value: number) {
    await this.initialize();
    // Round to nearest .5 degrees
    value = Math.round(value * 2) / 2;
    await this.doAPIRequest("", { target_temperature: value }, "put");
    this.thermostatData!.targetTemperature = value;
  }

  // Get thermostat data, collapse multiple requests into one promise
  private async refreshThermostat(emitUpdate = false) {
    if (emitUpdate) {
      this.log.debug("ThermoSmart API refresh", 2 * this.ttl);
    }
    if (this.refreshTimeout) {
      clearTimeout(this.refreshTimeout);
      this.refreshTimeout = null;
    }
    await this.initialize();

    if (this.thermostatData!.updatedAt + this.ttl * 60 * 1000 <= Date.now()) {
      this.apiPromise = this.apiPromise || this.doAPIRequest("");
      const result = await this.apiPromise;
      this.apiPromise = null;

      if (result) {
        this.thermostatData!.updatedAt = Date.now();
        this.thermostatData!.firmwareVersion = result.fw;

        const updates = this.updateThermostat(result);

        if (emitUpdate && updates.length) {
          this.emit("update", updates);
        }
      }
    }

    if (!this.refreshTimeout) {
      this.refreshTimeout = setTimeout(() => this.refreshThermostat(true), 2 * this.ttl * 60 * 1000);
    }
  }

  private onWebhookData(data: WebhookData) {
    if (!this.thermostatData) {
      // Not yet initialized, ignore incoming data
      return;
    }

    if (this.refreshTimeout) {
      clearTimeout(this.refreshTimeout);
    }

    const updates = this.updateThermostat(data);
    if (updates.length) {
      this.thermostatData.updatedAt = Date.now();
      this.emit("update", updates);
    }

    this.refreshTimeout = setTimeout(() => this.refreshThermostat(true), 2 * this.ttl * 60 * 1000);
  }

  // Update the thermostat data with incoming data, return what was updated
  private updateThermostat(data: UpdateData): Array<string> {
    const updates: Array<string> = [];

    if (data.room_temperature !== undefined && data.room_temperature !== this.thermostatData!.roomTemperature) {
      this.thermostatData!.roomTemperature = data.room_temperature;
      updates.push("room_temperature");
    }
    if (data.target_temperature !== undefined && data.target_temperature !== this.thermostatData!.targetTemperature) {
      this.thermostatData!.targetTemperature = data.target_temperature;
      updates.push("target_temperature");
    }
    if (data.source !== undefined) {
      const pause = data.source === "pause";
      if (pause !== this.thermostatData!.pause) {
        this.thermostatData!.pause = pause;
        updates.push("pause");
      }
    }
    if (data.ot !== undefined) {
      const ot = this.readOpenThermData(data.ot);
      // Check if relevenant OpenTherm data has changed
      if ((ot && (!this.thermostatData!.openThermData ||
            ot.centralHeatingActive !== this.thermostatData!.openThermData.centralHeatingActive ||
            ot.relativeModulationLevel !== this.thermostatData!.openThermData.relativeModulationLevel))
          || (!ot && this.thermostatData!.openThermData)) {
        updates.push("ot");
      }
      this.thermostatData!.openThermData = ot;
    }
    return updates;
  }

  private readOpenThermData(ot: APIOTData): OpenThermState {
    if (!ot.enabled) {
      return null;
    }
    const ot0 = parseInt(ot.raw.ot0) || 0;
    const ot17 = parseInt(ot.raw.ot17) || 0;
    return {
      // Bit 1 of the lower (slave) byte of ot0 is CH mode (0 = CH not active, 1 = CH active)
      centralHeatingActive: (ot0 & 0b10) > 0,
      // ot17 is the modulation percentage in f8.8 encoding floor(256 * floatvalue)
      relativeModulationLevel: ot17 / 256,
    };
  }

  private async doAPIRequest(
    call = "",
    params: Record<string, unknown> | undefined = undefined,
    method = "get"): Promise<APIResult | undefined> {
    if (!this.accessToken) {
      this.log.error("No access token set");
      return;
    }
    method = method.toUpperCase();

    // Construct the uri and headers
    const uri = new URL(this.apiURL);
    if (!uri.pathname.endsWith("/")) {
      uri.pathname += "/";
    }
    uri.pathname += `thermostat/${this.thermostat}`;
    if (call) {
      uri.pathname += `/${call}`;
    }
    uri.searchParams.set("access_token", this.accessToken);
    const headers = {};
    if (params) {
      headers["Content-Type"] = "application/json";
    }

    // Fetch the result
    this.log.debug("ThermoSmart API request", uri.toString(), method, params || "");
    const response = await fetch(uri.toString(), {
      method,
      headers,
      body: params ? JSON.stringify(params) : undefined,
    });
    if (!response.ok) {
      throw new Error(`Error in API call: ${response.statusText}`);
    }
    let result: APIResult | undefined;
    const contentType = response.headers.get("content-type");
    if (contentType && contentType.includes("application/json")) {
      result = await response.json();

      // Remove the 'schedule' and 'exceptions' we're not interested in
      if (typeof result === "object") {
        delete result.schedule;
        delete result.exceptions;
      }
    }

    this.log.debug("ThermoSmart API response", result);
    return result;
  }
}
