import { EventEmitter } from "events";
import { Server } from "http";
import { join } from "path";

import express from "express";
import { Logging } from "homebridge";
import moment from "moment-timezone";
import morgan from "morgan";
import { createStream } from "rotating-file-stream";

import { WebhookData } from "./api";


const WEBHOOK_IP = "127.0.0.1";
const WEBHOOK_PORT = 7512;


export class WebhookHandler extends EventEmitter {
  private readonly log: Logging;
  private readonly port: number;
  private readonly ip: string;
  private server: Server | null = null;
  private readonly app: express.Application;

  constructor(log: Logging, storagePath: string, port = WEBHOOK_PORT, ip = WEBHOOK_IP) {
    super();

    this.log = log;
    this.port = port;
    this.ip = ip;

    // Initialize an Express app
    this.app = express();

    // Setup logging
    const accessLogStream = createStream("webhook.log", {
      path: join(storagePath, "thermosmart"),
      // Match nginx defaults
      interval: "1d",
      rotate: 14,
      compress: true,
    });

    // Setup custom log format to log remote IP address instead of '127.0.0.1' behind a proxy server
    morgan.token("proxy-addr", req => {
      let forwardedFor = req.headers["x-forwarded-for"];
      if (Array.isArray(forwardedFor)) {
        forwardedFor = forwardedFor.shift();
      }
      return forwardedFor || req.socket.remoteAddress;
    });
    // Update date token to show time in local time zone instead of UTC
    morgan.token("date", () => {
      return moment().format("DD/MMM/y:HH:mm:ss ZZ");
    });
    morgan.format("proxyformat",
      ":proxy-addr - :remote-user [:date] \":method :url HTTP/:http-version\" :status :res[content-length] \":referrer\" \":user-agent\"");
    this.app.use(morgan("proxyformat", { stream: accessLogStream }));

    // Further app initialization
    this.app.use(express.json()); // Only listen for JSON requests
    this.app.disable("x-powered-by"); // Don't send X-Powered-By header

    // Add the POST '/events' endpoint
    this.app.post("/events", (req, res) => this.onRequest(req, res));
  }

  startServer() {
    if (!this.server) {
      this.log(`Starting webhook server on ${this.ip}:${this.port}`);
      this.server = this.app.listen(this.port, this.ip, () => {
        this.log.info("Webhook server listening for incoming events");
      }).on("error", error => this.log.error(`Webhook server error: ${error}`));
    }
  }

  stopServer() {
    if (this.server) {
      this.log.info("Stopping webhook server");
      this.server.close();
      this.server = null;
    }
  }

  private async onRequest(req: express.Request, res: express.Response) {
    // req.body is parsed JSON
    const incoming: WebhookData = req.body;
    // Remove the 'schedule' and 'exceptions' we're not interested in
    delete incoming.schedule;
    delete incoming.exceptions;
    this.log.debug("Got webhook data", incoming);

    // Handle the incoming data
    const data: WebhookData = { thermostat: incoming.thermostat };
    if ("room_temperature" in incoming) {
      data.room_temperature = incoming.room_temperature;
    }
    if ("target_temperature" in incoming) {
      data.target_temperature = incoming.target_temperature;
    }
    if ("source" in incoming) {
      data.source = incoming.source;
    }
    if ("ot" in incoming) {
      data.ot = incoming.ot;
    }

    // If we have anything interesting, emit it
    if (Object.keys(data).length) {
      this.emit("webhook:" + incoming.thermostat, data);
    }

    // Send an empty result
    res.status(204).send();
  }
}
